from pyrogram import Client, Filters

app = Client("my_account")


@app.on_message(Filters.me & Filters.regex("@everyone"))
def hello(client, message):
    text = ""
    for member in app.iter_chat_members(message.chat.id):
        if member.user.username != None:
            text += "@" + member.user.username + " "
        elif member.user.first_name != None:
            text += "<a href=\"tg://user?id=" + str(member.user.id) + "\">" + member.user.first_name + "</a> "
        else:
            text += "<a href=\"tg://user?id=" + str(member.user.id) + "\">WTF</a> "
    message.reply_text(text)

app.run()