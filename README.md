# `@everyone` - small userbot that helps you mention every user in the chat

## Installation

```bash
pip3 install pyrogram
```

## Usage


- Open console and type:
    ```bash
    python main.py
    ```
- Complete authorization in the console.
- Send `@everyone` to any chat and wait for a second!
- Bada Boom)